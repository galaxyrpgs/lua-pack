-- encoding = UTF-8
local module = {
   LANG_SELECT_TEXT     = "Language:",
   ON_OFF_TEXT          = "Render closest player",
   SAVEBUTTON_TEXT      = "Save",
   RESETBUTTON_TEXT     = "Default",
   CHOOSEPOSBUTTON_TEXT = "Choose position",
   ALIGNMENT_TEXT       = "Alignment:",
   ALIGN_LEFT_TEXT      = "Left side",
   ALIGN_MIDDLE_TEXT    = "Center",
   ALIGN_RIGHT_TEXT     = "Right side",
   CHEKBOX_TEXT_KLIST   = "Use Kill-List colour",
   CURRENT_WORD_TEXT    = "Сurrent displayed phrase:",
   NOTFOUND_WORD_TEXT   = "Noone's by your side:",
   FONT_SETTINGS_TEXT   = "Font: ",
   FONT_NAME_TEXT       = "Name",
   FONT_SIZE_TEXT       = "Size",
   FONT_FLAG_TEXT       = "Style",
   FONT_COLOR_TEXT      = "Colour",
   EXCEPTION_TEXT       = "Exceptional players:",
}
return module