---   encoding = cp1251
local script_name = "RenderClosest"

local default_tbl = {
   settings = {
      i_lang  = 1,
      enabled = true,
      i_pos_x = 300,
      i_pos_y = 300,
      word = "Closest player: ",
      not_found = "Not found",
      font = "Arial",
      i_size = 10,
      i_flag = 13,
      color_font = 0xFFFFFFFF,
      klist_color = true,
      i_alignment = 1
   },
   team = {
      "sereja_savushkin"
   }
}
 
inicfg.save(default_tbl, script_name .. "\\default.ini")