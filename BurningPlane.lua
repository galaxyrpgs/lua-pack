script_name("BurningPlane")
script_author("THERION")
script_description("Suckless plane death countdown for skilled pilots")

local PLANE_BURN_TIME    = 25
local DEATH_SCENE_LENGTH = 2

-- change these settings if you don't like em
local config = {
   pos        = {x = 0.5, y = 0.2}, -- { 0.5, 0.5 } = middle of the screen
   scale      = {x = 1.5, y = 2.4},
   color      = { a = 0xFF, r = 0x00, g = 0xA0, b = 0x00 },
   shadow     = { a = 0xFF, r = 0x00, g = 0x00, b = 0x00 },
   style      = "MENU",
   align      = "CENTER",
   outline    = 1,
   precise    = true,
}

-- your screen resolution
local screen_x, screen_y = getScreenResolution()

--- the only library you need
local mad = require("MoonAdditions")

--- Simplified version of MoonAdditions draw_text function but not as big
--  color, shadow = {a = ..., r = ..., g = ..., b = ...} tables representing the colors,
--  pos,   scale = {x = ..., y ...} vectors. Position is relative to screen resolution
local function draw_text(text, pos, scale, color, shadow, mad_style, mad_align, outline)
   local bounds = 10000 * (mad_align == mad.font_align.RIGHT and -1 or 1)
   mad.draw_text(text, pos.x, pos.y, mad_style, scale.x, scale.y, mad_align, bounds, true, false, 
   color.r, color.g, color.b, color.a, outline, 0, shadow.r, shadow.g, shadow.b, shadow.a, false)
end

--- it draws the countdown
local draw_timer = lua_thread.create_suspended(
function(veh, health)
   local clock = os.clock()
   local timer = PLANE_BURN_TIME

   local pos_real = {
      x = screen_x * config.pos.x,
      y = screen_y * config.pos.y,
   }
   -- pre-calculated text position for your resolution 
   local text = tostring(PLANE_BURN_TIME)

   repeat wait(0)
      
      timer = PLANE_BURN_TIME + clock - os.clock()
      text = config.precise and string.format("%.02f", timer) or tostring(math.floor(timer))
      
      draw_text(text, pos_real, config.scale, config.color, config.shadow, 
      mad.font_style[config.style], mad.font_align[config.align], config.outline)

   until timer <= 0 or isCarDead(veh)

   -- death scene timer if you accidentaly crashed into an unloaded tree
   clock = os.clock()
   while clock - os.clock() < DEATH_SCENE_LENGTH do 
      wait(0)
   end
end)

function main()
   repeat wait(0) until isSampAvailable()

   while true do wait(0)
      local plane_or_heli = isCharInAnyPlane(PLAYER_PED) or isCharInAnyHeli(PLAYER_PED)

      if draw_timer:status() == "yielded" then
         if not plane_or_heli then
            draw_timer:terminate()
         end 
      else 
         if plane_or_heli then
            local veh = storeCarCharIsInNoSave(PLAYER_PED)
            local health = getCarHealth(veh)
            if health < 250 then
               draw_timer:run(veh, health)
            end
         end
      end
   end
end