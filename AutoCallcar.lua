script_name("AutoCallcar")
script_author("THERION")
script_url("https://gitlab.com/THXRION666/lua-pack")

local GALAXY_IP  = {"176.32.39.200", "176.32.39.199", "176.32.39.198"}
---
local trigger    = "предлагает тебе приобрести автомобиль"
local accept_cmd = "/accept car"
local sell_cmd   = "/sellcar"
---

local inicfg = require("inicfg")
local ev = require("samp.events")

local ini = inicfg.load({
   GENERAL = {
      accept_delay = 60,
      sellcar_key = 90, 
      cost = 1000
   }
}, thisScript().name .. ".ini")

local function is_in_array(array, value)
   for _, element in ipairs(array) do
      if value == element then
         return true
      end
   end
   return false
end

ev.onServerMessage = function(color, text)
   if text:find(trigger) then
      lua_thread.create(
      function() 
         wait(ini.GENERAL.accept_delay) 
         sampSendChat(accept_cmd) 
      end)
   end
end

local function sell(id)
   local result_msg = string.format("%s %d %d", sell_cmd, id, ini.GENERAL.cost)
   sampSendChat(result_msg)
end

function main()
   do
      if not doesDirectoryExist("moonloader\\config") then 
         createDirectory("moonloader\\config") 
      end

      if not doesFileExist(thisScript().name .. ".ini") then 
         inicfg.save(ini, thisScript().name .. ".ini") 
      end
   end

   while not isSampAvailable() do wait(0) end

   do
      local ip, _ = sampGetCurrentServerAddress()
      if not is_in_array(GALAXY_IP, ip) then
         thisScript():unload()
      end
   end

   while true do wait(0)
      if isKeyJustPressed(ini.GENERAL.sellcar_key) and isCharInAnyCar(PLAYER_PED) and not sampIsChatInputActive() and not isSampfuncsConsoleActive() then
         local veh = storeCarCharIsInNoSave(PLAYER_PED)
         local driver = getDriverOfCar(veh)
         
         if driver and driver ~= PLAYER_PED then 
            local _, dr_id = sampGetPlayerIdByCharHandle(driver)
            sell(dr_id)
         else
            for _, hndl in pairs(getAllChars()) do
               if hndl ~= PLAYER_PED and isCharInAnyCar(hndl) and storeCarCharIsInNoSave(hndl) == veh then
                  local possible, id = sampGetPlayerIdByCharHandle(hndl)
                  if possible then
                     sell(id)
                     break
                  end
               end
            end
         end
      end
   end
end