script_name("AutoEngine")
script_description("���� ������� ��� � �������� �� � 24/7")
script_url("https://gitlab.com/THXRION666/lua-pack")

-- this data should be up to date for proper work
local GALAXY_IP = {"176.32.39.200", "176.32.39.199", "176.32.39.198"}
local trigger_text = {
   "^����� ������� ���������, ������ %{33AA33%}%/engine%{FFFFFF%} ��� ����� ������� %{33AA33%}NUM 4%{FFFFFF%}.$",
   "^��������� ������.$",
   "^��������� ������ ��������. ������ %/mechanic ��� ������ ��������, ���� %/taxi ��� ������ �����.$",
   "^����� ������� ��������� �� �����, ������ %/lock.$",
   "^�������. ����������� � �����, ���������� �� �����.$",
}
-- 
-- config
local restart_delay = 60
local start_cmd = "/engine"
-- config

local ev = require("samp.events")

local start_thr = lua_thread.create_suspended(
function() 
   wait(restart_delay) 
   sampSendChat(start_cmd) 
end)

local function is_in_array(array, value)
   for _, element in ipairs(array) do
      if type(value) == "string" then
         if value:find(element) then
            return true
         end
      else
         if value == element then
            return true
         end
      end
   end
   return false
end

ev.onServerMessage = function(color, text)
   if isCharInAnyCar(PLAYER_PED) then
      if is_in_array(trigger_text, text) then
         start_thr:run()
      end
   end
end

function main()
   while not isSampAvailable() do wait(0) end

   do
      local ip, _ = sampGetCurrentServerAddress()
      if not is_in_array(GALAXY_IP, ip) then
         thisScript():unload()
      end
   end


	wait(-1)
end