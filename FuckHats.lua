script_author("THERION")
script_dependencies("SAMPFUNCS")
script_description("Ignores most player-attached objects")

local bones = {1, 2, 15, 16, 17, 18}
--SPINE, HEAD, LEFT N RIGHT SHOULDERS, NECK, JAW

onReceiveRpc = 
function (packet_id, bs)
   if packet_id == 113 then
      raknetBitStreamIgnoreBits(bs, 48)            --UINT16 + UINT32
      local create = raknetBitStreamReadBool(bs)
      if create then 
         raknetBitStreamIgnoreBits(bs, 32)         --UINT32
         local bone = raknetBitStreamReadInt32(bs)
         for _, i in ipairs(bones) do 
            if bone == i then return false end
         end
      end
   end
end

function main() 
   wait(-1) 
end
