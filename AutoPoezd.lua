script_name("AutoPoezd")
script_author("THERION")
script_description("������������� ������� ������� � ���������� ����� � ������ ��� ���� �����")
script_dependencies("MoonAdditions", "SAMP.lua", "inicfg")
script_url("https://t.me/flugegeheiman666")

-- DATA

local GALAXY_IP = {"176.32.39.200", "176.32.39.199", "176.32.39.198"}

local veh_list = { -- every gang spray and blip color by platenumber
   ["LCN 001"] = { spray = 0x057F94, blip = 0x057F94FF }, -- ��������
   ["YKZ 001"] = { spray = 0xFAFB71, blip = 0xFAFB71FF }, -- �������
   ["RMA 001"] = { spray = 0x778899, blip = 0x778899FF }, -- ������� �����
   ["BLS 001"] = { spray = 0x8A2CD7, blip = 0x8A2CD7FF }, -- ������
   ["VGS 001"] = { spray = 0xFFD720, blip = 0xFFD720FF }, -- ���� ���� ��� � ���
   ["ELC 001"] = { spray = 0x0FD9FA, blip = 0x0FD9FAFF }, -- ����������
   ["GRS 001"] = { spray = 0x10DC29, blip = 0x10DC29FF }, -- ������
   ["SRS 001"] = { spray = 0x6495ED, blip = 0x6495EDFF }, -- �������� �������
   ["SFR 001"] = { spray = 0x20D4AD, blip = 0x20D4ADFF }, -- �����
   ["TRD 001"] = { spray = 0xFA24CC, blip = 0xFA24CCFF }, -- �����������
   ["HA 001"]  = { spray = 0x70524D, blip = 0x70524DFF }, -- ������� �����
   ["BLK 001"] = { spray = 0x4C436E, blip = 0x4C436EFF }, -- ������ ����
}

-- prints message to SAMP Chat
local function log(msg)
   local format = "{FB4343}[%s]{FFFFFF}: %s{FFFFFF}."
   sampAddChatMessage(string.format(format, thisScript().name, msg), -1)
end

-- prints out if user does not have some of the needed files
local function catch_exception(status, path)
   local msg = string.format('File not found: "%s". Shutting down..', path)
   if not status then log(msg) end
   assert(status, msg)
end

do --Loading libraries
   local list = {
      mad        = "MoonAdditions",
      sampev     = "samp.events",
      inicfg     = "inicfg",
   }

   local result = nil
   for var, path in pairs(list) do
      result, _G[var] = pcall(require, path)
      catch_exception(result, path)
   end
end

local ini = inicfg.load({
   SETTINGS = {
      markers = true,
      recolor = true
   }
}, thisScript().name .. ".ini")


local function split_rgb(hex)
   return
      bit.band(bit.rshift(hex, 16), 255), 
      bit.band(bit.rshift(hex, 8), 255), 
      bit.band(hex, 255)
end

local function is_in_array(array, value)
   for _, element in ipairs(array) do
      if value == element then
         return true
      end
   end
   return false
end

local function respray(car, new_r, new_g, new_b)
   for _, comp in ipairs(mad.get_all_vehicle_components(car)) do
      for _, obj in ipairs(comp:get_objects()) do
         for _, mat in ipairs(obj:get_materials()) do
            local r, g, b, a = mat:get_color()
            if (r == 0x3C and g == 0xFF and b == 0x00) or (r == 0xFF and g == 0x00 and b == 0xAF) then
               mat:set_color(new_r, new_g, new_b, a)
            end
         end
      end
   end
end

sampev.onSetVehicleNumberPlate = function(veh_id, text)
   if ini.SETTINGS.markers then
      for index, color_data in pairs(veh_list) do
         if text:find(index) then
            lua_thread.create(
            function()
               wait(0)
               local _, car = sampGetCarHandleBySampVehicleId(veh_id)
               local blip = addBlipForCar(car)
               changeBlipColour(blip, color_data.blip)
               repeat wait(0) until not ini.SETTINGS.markers or not sampGetCarHandleBySampVehicleId(veh_id)
               removeBlip(blip)
            end)
         end
      end
   end
   if ini.SETTINGS.recolor then
      for index, color_data in pairs(veh_list) do
         if text:find(index) then
            lua_thread.create(
            function()
               wait(0)
               local _, car = sampGetCarHandleBySampVehicleId(veh_id)
               respray(car, split_rgb(color_data.spray))
            end)
         end
      end
   end
end

function main()
   do
      if not doesDirectoryExist("moonloader\\config") then 
         createDirectory("moonloader\\config") 
      end

      if not doesFileExist(thisScript().name .. ".ini") then 
         inicfg.save(ini, thisScript().name .. ".ini") 
      end
   end

   while not isSampAvailable() do wait(0) end

   do
      local ip, _ = sampGetCurrentServerAddress()
      if not is_in_array(GALAXY_IP, ip) then
         thisScript():unload()
      end
   end

   sampRegisterChatCommand("carblip",
   function()
      ini.SETTINGS.markers = not ini.SETTINGS.markers
      if inicfg.save(ini, "moonloader\\config\\" .. thisScript().name .. ".ini") then
         log("����������� �������� " .. (ini.SETTINGS.markers and "{5BFF83}��������" or "{FB4343}���������"))
      end
   end)
   sampRegisterChatCommand("carrecolor",
   function()
      ini.SETTINGS.recolor = not ini.SETTINGS.recolor
      if inicfg.save(ini, "moonloader\\config\\" .. thisScript().name .. ".ini") then
         log("��������� ����� �������� " .. (ini.SETTINGS.recolor and "{5BFF83}��������" or "{FB4343}���������"))
      end
   end)

   wait(-1)
end